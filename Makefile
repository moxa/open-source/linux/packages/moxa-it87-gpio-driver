KRELEASE ?= $(shell uname -r)
KBUILD ?= /lib/modules/$(KRELEASE)/build
obj-m := gpio-it87.o

modules:
	$(MAKE) -C $(KBUILD) M=$(PWD) modules

install: modules
	/usr/bin/install -m 644 -D gpio-it87.ko /lib/modules/$(KRELEASE)/kernel/drivers/gpio/gpio-it87.ko
	/usr/bin/install -m 644 -D gpio-it87.conf /usr/lib/modules-load.d/gpio-it87.conf

	@$(eval TAR_NAME=$(shell sh -c "dmidecode -t 12 | grep EXPCF2000"))
	@if [ ! -z "$(findstring EXPCF2000, $(TAR_NAME))" ] ; then /usr/bin/install -m 644 -D gpio_it87_expcf2000.conf /lib/modprobe.d/gpio_it87_expcf2000.conf ; fi

clean:
	$(MAKE) -C $(KBUILD) M=$(PWD) clean
